DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `account`
--

INSERT INTO `account` (`id`, `last_name`, `name`) VALUES
(1, 'Perez', 'Juan'),
(2, 'Perez', 'Lucas'),
(3, 'Martinez', 'Maria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tweets`
--

CREATE TABLE `tweets` (
  `id` int(11) NOT NULL,
  `text` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `account_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_reply` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tweets`
--

INSERT INTO `tweets` (`id`, `text`, `status`, `account_id`, `created_at`, `in_reply`) VALUES
(1, 'texto prueba 1', 1, 1, '2019-03-15 16:51:09', NULL),
(2, 'texto prueba 2', 1, 2, '2019-03-15 16:51:09', '1'),
(3, 'texto prueba 3', 1, 1, '2019-03-15 16:51:11', NULL),
(4, 'texto prueba 4', 1, 2, '2019-03-15 16:51:11', '1'),
(5, 'texto prueba 5', 1, 3, '2019-03-15 16:52:34', NULL),
(6, 'texto prueba 6', 1, 3, '2019-03-15 16:52:34', NULL),
(7, 'texto prueba 7', 1, 3, '2019-03-15 16:52:36', NULL),
(8, 'texto prueba 8', 1, 3, '2019-03-15 16:52:36', NULL),
(9, 'texto prueba 9', 1, 2, '2019-03-15 16:53:37', NULL),
(10, 'texto prueba 11', 1, 3, '2019-03-15 16:53:37', NULL),
(11, 'texto prueba 12', 1, 2, '2019-03-15 16:53:39', NULL),
(12, 'texto prueba 10', 1, 3, '2019-03-15 16:53:39', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tweets_1_idx` (`account_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tweets`
--
ALTER TABLE `tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tweets`
--
ALTER TABLE `tweets`
  ADD CONSTRAINT `fk_tweets_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;