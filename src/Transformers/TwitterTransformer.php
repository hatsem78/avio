<?php
/**
 * Created by PhpStorm.
 * User: octavio
 * Date: 18/03/19
 * Time: 21:23
 */
namespace App\Transformers;
use App\Models\Twitter;
use Illuminate\Database\Eloquent\Collection;

class TwitterTransformer
{
    /**
     * @param Twitter $twitter
     *
     * @return array
     */
    public function transform(Twitter $twitters)
    {
        return [
            'id'          => $twitters->id,
            'text'   => $twitters->text,
            'status'     => $twitters->status,
            'account_id'       => $twitters->account_id,
            'created_at' => $twitters->created_at,
            'in_reply'  => $twitters->in_reply,
        ];
    }
    /**
     * @param Collection $twitters
     *
     * @return array
     */
    public function transformCollection(Collection $twitters)
    {
        $data = array();
        foreach ($twitters as $twitter) {
            $data[] = TwitterTransformer::transform($twitter);
        }
        return $data;
    }
}