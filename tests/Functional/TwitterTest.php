<?php
/**
 * Created by PhpStorm.
 * User: octavio
 * Date: 18/03/19
 * Time: 21:15
 */

namespace Tests\Functional;

class TwitterTest extends BaseTestCase
{
    /**
     * @test
     */
    public function getTwitterEndpoint()
    {
        $response = $this->runApp('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
    }
    /**
     * @test
     */
    public function getUsersCollectionEndpoint()
    {
        $response = $this->runApp('GET', '/api/v1/twitter');
        $collection = (array) json_decode($response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertArrayHasKey('total_results', $collection);
    }
}